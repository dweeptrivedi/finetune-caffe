Finetune pre-trained models in caffe without writing any code. Let's start with googleNet model.

---

##Files:

1. Model graph: train_val.prototxt

2. Solver: solver.prototxt

3. Model weights: bvlc_googlenet.caffemodel

4. image list: train.txt
	
---		

## edits:

**model graph: train_val.prototxt**  (See diff_sample.txt for diff sample)

***Modify location of train.txt and test.txt***

- First layer of train_val.prototxt looks like this:

```	
layer {
  name: "data"
  type: "ImageData"
  top: "data"
  top: "label"
  include {
	phase: TRAIN
  }
  transform_param {
	mirror: true
	crop_size: 224
	mean_file: "/home/ubuntu/caffe-0.15.9/data/ilsvrc12/imagenet_mean.binaryproto"
  }
  image_data_param {
	source: "/data/projects/Graffiti/phase2_part2/googleNet/train.txt"
	batch_size: 64
	new_height: 256
	new_width: 256
  }

}
```

- Change `image_data_param.source` to point to your train.txt
	
- `phase: TRAIN` means this layer is specific to training. Make similar changes for layer with `phase: TEST` to point it to your test.txt






***Modify number of classes at 3 output layers in googleNet model***

Go to Following 3 layers with:
	
1. `name: "loss1/classifier_ft"` 
	
2. `name: "loss2/classifier_ft"`
	
3. `name: "loss3/classifier_ft"`

For all 3 layers, change `inner_product_param.num_output: 15` to the number of classes in your dataset




**solver: solver.prototxt**

No need to change anything unless you want to tune hyperparameters.



**image list: train.txt**

You need to generate this based on the dataset. I.e., if you are training classifier for Wall subclasses  then you need this file in following format:

```
<image_path> <integer_label>
```

see train_sample.txt and test_sample.txt for example.


---

##Training:

After above edits are done, execute `run.sh`

--

##Testing:

`caffe test -model train_val.prototxt -weights bvlc_googlenet_iter_1000.caffemodel -gpu 1 -iterations 82`

