START=$(date +%s.%N)
date
/home/ubuntu/caffe-0.15.9/build/tools/caffe train -solver solver.prototxt -weights bvlc_googlenet.caffemodel -gpu 0,1 &> temp.txt
date
END=$(date +%s.%N)
DIFF=$(echo "$END - $START" | bc)
echo $DIFF > total_time.txt
echo $DIFF
